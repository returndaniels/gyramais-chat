# gyramais-test
Desafio para vaga Fullstack developer GYRA+ - Backend

## Instalação
Use o git [clone](https://git-scm.com/docs/git-clone) para clonar o repositório. Então instale as dependencias com yarn.
```bash
git clone https://gitlab.com/returndaniels/gyramais-chat.git
cd backend

npm install
```

## :arrow_forward: Scripts disponíveis

No diretório do projeto, você pode executar:

### `npm start` ou `npm run dev`

Executa a aplicação em modo de desenvolvimento.<br />
Abra [http://localhost:3000](http://localhost:3000) para abrir no navegador.

## Dependencias
NodeJS | Express | SocketIO | Mongoose | dotenv | body-parser